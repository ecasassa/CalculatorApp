//
//  ViewController.swift
//  CalculatorApp
//
//  Created by Etienne Casassa on 11/1/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numbersDisplayed: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func numberOne(_ sender: UIButton) {
        numbersDisplayed.text = String(1)
    }
    
    @IBAction func numberTwo(_ sender: UIButton) {
        numbersDisplayed.text = String(2)
    }
    
    @IBAction func numberThree(_ sender: UIButton) {
        numbersDisplayed.text = String(3)
    }
    
    @IBAction func numberFour(_ sender: UIButton) {
        numbersDisplayed.text = String(4)
    }
    
    @IBAction func numberFive(_ sender: UIButton) {
        numbersDisplayed.text = String(5)
    }
    
    @IBAction func numberSix(_ sender: UIButton) {
        numbersDisplayed.text = String(6)
    }
    
    @IBAction func numberSeven(_ sender: UIButton) {
        numbersDisplayed.text = String(7)
    }
    
    @IBAction func numberEight(_ sender: UIButton) {
        numbersDisplayed.text = String(8)
    }
    
    @IBAction func numberNine(_ sender: UIButton) {
        numbersDisplayed.text = String(9)
    }
    
    @IBAction func numberZero(_ sender: UIButton) {
        numbersDisplayed.text = String(0)
    }
    
    @IBAction func symbolPlus(_ sender: UIButton) {

    }
    
    @IBAction func symbolMinus(_ sender: UIButton) {
    }
    
    @IBAction func symbolMultiply(_ sender: UIButton) {
    }
    
    @IBAction func symbolDivide(_ sender: UIButton) {
    }
    
    @IBAction func symbolEqual(_ sender: UIButton) {
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        numbersDisplayed.text = ""
        self.loadView()
    }

}

